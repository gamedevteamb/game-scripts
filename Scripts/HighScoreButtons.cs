﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HighScoreButtons : MonoBehaviour {

    public void LoadMainMenu()
    {
        //Load the next scene which is the first level
        SceneManager.LoadScene("MainMenu");
    }
}
