﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class EndScreenButtons : MonoBehaviour {

    public void QuitGame()
    {
        Application.Quit();
    }

    public void LoadMainMenu()
    {
        //Load the next scene which is the first level
        SceneManager.LoadScene("MainMenu");
    }
}
