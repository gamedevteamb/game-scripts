﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuButtons : MonoBehaviour
{
   
	public void PlayGame ()
    {
        //Load the next scene which is the first level
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);	
	}

    public void HighScore()
    {
        //Load the next scene which is the first level
        SceneManager.LoadScene("HighScore");
    }

    public void QuitGame()
    {
        Application.Quit();
    }

}
