﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Player : MonoBehaviour {

    public float speed;
    public Text scoreCount;

    private Rigidbody rb;
    private int score;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        score = 0;
        //SetScoreCount ();
    }

    void FixedUpdate ()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

        GetComponent<Rigidbody>().AddForce(movement * speed * Time.deltaTime);
        {
            if (Input.GetKeyDown("space") && GetComponent<Rigidbody>().transform.position.y <= 10.6250001f)
            {
                Vector3 jump = new Vector3(0.0f, 400.0f, 0.0f);

                GetComponent<Rigidbody>().AddForce(jump);
            }

            if (Input.GetKeyDown("g"))
            {
                Vector3 dash = new Vector3(0.0f, 0.0f, 500.0f);

                GetComponent<Rigidbody>().AddForce(dash);
            }
        }

        rb.AddForce (movement * speed);
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("PickUp"))
        {
            other.gameObject.SetActive(false);

            score = score + 1;
            //SetScoreCount ();
        }
    }

    //void SetScoreCount ()
    //{
    //    scoreCount.text + "Score: " + score.ToString();
    //}
}